# Chat Service

The live services accepts websocket connections and routes messages between connected users.

## Documentation

Production: https://connect.kominal.com/live-service/api-docs
Test: https://connect-test.kominal.com/live-service/api-docs
