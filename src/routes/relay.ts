import Router from '@kominal/service-util/helper/router';
import { entrypoints } from '../check';
import { critical } from '@kominal/service-util/helper/log';

const router = new Router();

/**
 * Finds a suitable media relay server.
 * @group Protected
 * @security JWT
 * @route GET /relay
 * @consumes application/json
 * @produces application/json
 * @returns {string} 200 - The ip of a single running relay service.
 * @returns {Error}  401 - Unauthorized - Missing authorization header
 * @returns {Error}  406 - Not Acceptable - A parameter is missing or is invalid or the request could not be processed
 * @returns {Error}  500 - Internal Server Error
 */
router.getAsUser('/relay', async (req, res) => {
	if (entrypoints.length > 0) {
		const entrypoint = entrypoints[Math.floor(Math.random() * entrypoints.length)];
		res.status(200).send(entrypoint);
	} else {
		critical('No working host!');
		res.status(404).send();
	}
});

export default router.getExpressRouter();
