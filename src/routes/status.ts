import Router from '@kominal/service-util/helper/router';
import StatusDatabase from '@kominal/connect-models/status/status.database';
import service from '..';

const router = new Router();

/**
 * Set the new status for a user.
 * @group Protected
 * @security JWT
 * @route POST /status
 * @consumes application/json
 * @produces application/json
 * @param {string} status.body.required - The new status. Possible values: OFFLINE, AWAY_FROM_KEYBOARD, DO_NOT_DISTURB, ONLINE
 * @returns {void} 200 - The status was updated
 * @returns {Error}  401 - Unauthorized - Missing authorization header
 * @returns {Error}  406 - Not Acceptable - A parameter is missing or is invalid or the request could not be processed
 * @returns {Error}  500 - Internal Server Error
 */
router.postAsUser('/status', async (req, res, userId) => {
	const status = req.body.status;
	if (!['OFFLINE', 'AWAY_FROM_KEYBOARD', 'DO_NOT_DISTURB', 'ONLINE'].includes(req.body.status)) {
		throw 'error.status.invalid';
	}

	if ((await StatusDatabase.countDocuments({ userId, status })) === 0) {
		await StatusDatabase.findOneAndUpdate({ userId }, { status }, { upsert: true });
		service.getSMQClient()?.publish('TOPIC', `PUBLIC.USER.${userId}`, 'STATUS', { userId, status });
	}

	res.status(200).send();
});

export default router.getExpressRouter();
