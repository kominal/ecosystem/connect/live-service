import { info, error } from '@kominal/service-util/helper/log';
const isPortReachable = require('is-port-reachable');
export let entrypoints: string[] = [];

export default async function check() {
	info('Checking health of relay services...');

	const newEntrypoints: string[] = [];
	for (const relayService of ['94.130.74.215', '78.47.94.162', '95.217.17.182']) {
		try {
			const reachable = await isPortReachable(3478, { host: relayService });
			if (reachable) {
				newEntrypoints.push(relayService);
			} else {
				error(`Relay service on '${relayService}' is unreachable.`);
			}
		} catch (err) {
			error(`Could not resolve ip for '${relayService}'.`);
		}
	}
	entrypoints = newEntrypoints;
}
