import service from '..';
import { SocketConnectionDatabase } from '@kominal/connect-models/socketconnection/socketconnection.database';
import { Connection } from '../connection';
import { TASK_ID } from '@kominal/service-util/helper/environment';

export class CallHandler {
	static async onUserConnect(connection: Connection) {
		const { interestingGroupIds } = connection;

		for (const groupId of interestingGroupIds) {
			const members: any[] = [];
			for (const socketConnection of await SocketConnectionDatabase.find({ groupId })) {
				members.push({
					connectionId: socketConnection.get('connectionId'),
					userId: socketConnection.get('userId'),
				});
			}
			if (members.length > 0) {
				connection.socket.emit('EVENT', { type: 'CALL.MEMBERS', body: { groupId, members } });
			}
		}
	}

	static async onUserRefresh(connection: Connection, removedInterestingGroupIds: string[], addedInterestingGroupIds: string[]) {
		const { connectionId, userId } = connection;

		if (!connectionId || !userId) {
			return;
		}

		const socketConnection = await SocketConnectionDatabase.findOne({ taskId: TASK_ID, connectionId, userId });
		if (!socketConnection) {
			return;
		}

		const groupId = socketConnection.get('groupId');

		if (userId && groupId && removedInterestingGroupIds.includes(groupId)) {
			await this.onCallLeave(connectionId, userId);
		}

		for (const groupId of removedInterestingGroupIds) {
			connection.socket.emit('EVENT', { type: 'CALL.MEMBERS', body: { groupId, members: [] } });
		}

		for (const groupId of addedInterestingGroupIds) {
			const members: any[] = [];
			for (const socketConnection of await SocketConnectionDatabase.find({ groupId })) {
				members.push({
					taskId: socketConnection.get('taskId'),
					connectionId: socketConnection.get('connectionId'),
					userId: socketConnection.get('userId'),
				});
			}
			if (members.length > 0) {
				connection.socket.emit('EVENT', { type: 'CALL.MEMBERS', body: { groupId, members } });
			}
		}
	}

	static async onUserDisconnect(connectionId: string, userId: string, groupId?: string) {
		await this.onCallLeave(connectionId, userId, groupId);
	}

	static async onEvent(connection: Connection, event: { type: string; body?: any }): Promise<void> {
		const { connectionId, userId } = connection;
		const { type, body } = event;
		if (type === 'CALL.JOIN') {
			this.onCallJoin(connection, body.groupId);
		} else if (type === 'CALL.LEAVE') {
			if (connectionId && userId) {
				this.onCallLeave(connectionId, userId);
			}
		} else if (type === 'CALL.SIGNALING') {
			this.onCallSignaling(connection, body);
		}
	}

	static async onCallJoin(connection: Connection, groupId: string) {
		const { connectionId, userId, interestingGroupIds } = connection;

		if (!connectionId || !userId) {
			return;
		}

		await this.onCallLeave(connectionId, userId);

		if (interestingGroupIds.includes(groupId)) {
			await SocketConnectionDatabase.updateOne({ connectionId, taskId: TASK_ID }, { groupId });
			const smqClient = service.getSMQClient();
			if ((await SocketConnectionDatabase.countDocuments({ groupId })) > 1) {
				smqClient.publish('TOPIC', `DIRECT.GROUP.${groupId}`, 'CALL.JOIN', { groupId, userId, connectionId });
			} else {
				smqClient.publish('TOPIC', `DIRECT.GROUP.${groupId}`, 'CALL.START', { groupId, userId, connectionId });
			}
		}
	}

	static async onCallSignaling(
		connection: Connection,
		body: {
			taskId: string;
			connectionId: string;
			userId: string;
			groupId: string;
			targetConnectionId: string;
			signaling: string;
		}
	) {
		const { connectionId, userId } = connection;
		const { targetConnectionId, signaling } = body;

		const socketConnection = await SocketConnectionDatabase.findOne({ taskId: TASK_ID, connectionId });
		if (socketConnection) {
			const groupId = socketConnection.get('groupId');
			const targetSocketConnection = await SocketConnectionDatabase.findOne({ groupId, connectionId: targetConnectionId });
			if (targetSocketConnection) {
				service
					.getSMQClient()
					.publish('TOPIC', `DIRECT.CONNECTION.${targetConnectionId}`, 'CALL.SIGNALING', { userId, connectionId, groupId, signaling });
			}
		}
	}

	static async onCallLeave(connectionId: string, userId: string, groupId?: string) {
		if (!groupId) {
			const socketConnection = await SocketConnectionDatabase.findOne({ taskId: TASK_ID, connectionId });
			if (socketConnection) {
				groupId = socketConnection.get('groupId');
			}
		}

		if (groupId) {
			await SocketConnectionDatabase.updateOne({ taskId: TASK_ID, connectionId }, { groupId: undefined });
			service.getSMQClient().publish('TOPIC', `DIRECT.GROUP.${groupId}`, 'CALL.LEAVE', { groupId, userId, connectionId });
		}
	}
}
