import io from 'socket.io';
import { verify } from 'jsonwebtoken';
import { JWT_KEY, TASK_ID, STACK_NAME, SERVICE_TOKEN } from '@kominal/service-util/helper/environment';
import service from './index';
import axios from 'axios';
import { SocketConnectionDatabase } from '@kominal/connect-models/socketconnection/socketconnection.database';
import { StatusHandler } from './handler/handler.status';
import { debug } from '@kominal/service-util/helper/log';
import { CallHandler } from './handler/handler.call';

export const connections: Connection[] = [];

export class Connection {
	public interestingUserIds: string[] = [];
	public interestingGroupIds: string[] = [];

	public connectionId: string | undefined;
	public userId: string | undefined;

	public groupId: string | undefined;

	constructor(public socket: io.Socket) {
		this.connectionId = socket.id;
		socket.on('REGISTER', (jwt) => this.onRegister(jwt));
		socket.on('UNREGISTER', () => this.onUnregister());
		socket.on('disconnect', () => this.onUnregister());

		socket.on('PUBLISH', (packet) => this.onPublish(packet));
	}

	async onRegister(jwt: any) {
		if (jwt == null) {
			return;
		}
		const decodedToken = <any>verify(jwt, JWT_KEY);
		if (decodedToken.exp > Date.now()) {
			await this.onUnregister();
			this.userId = decodedToken.userId;
			connections.push(this);
			await this.refreshInterestingValues('CONNECT');

			const { connectionId, userId } = this;
			await SocketConnectionDatabase.updateOne({ taskId: TASK_ID, userId, connectionId }, {}, { upsert: true });

			debug(`Registering user ${userId} for connection ${connectionId}...`);

			const smqClient = service.getSMQClient();
			smqClient.subscribe('TOPIC', `DIRECT.CONNECTION.${connectionId}`);
			smqClient.subscribe('TOPIC', `DIRECT.USER.${userId}`);
			smqClient.subscribe('TOPIC', `INTERNAL.REFRESH.${userId}`);

			if (this.connectionId && this.userId) {
				await StatusHandler.onUserConnect(this);
				await CallHandler.onUserConnect(this);
			}
		}
	}

	async onUnregister() {
		if (connections.includes(this)) {
			connections.splice(connections.indexOf(this), 1);
		}
		const { connectionId, userId } = this;
		if (userId && connectionId) {
			debug(`Unregistering user ${userId} for connection ${connectionId}...`);

			await this.refreshInterestingValues('DISCONNECT');

			await CallHandler.onUserDisconnect(connectionId, userId);
			await StatusHandler.onUserDisconnect(userId);

			await SocketConnectionDatabase.deleteMany({ taskId: TASK_ID, userId, connectionId });

			const smqClient = service.getSMQClient();
			smqClient.unsubscribe('TOPIC', `DIRECT.CONNECTION.${connectionId}`);
			if (!connections.find((c) => c.userId === userId)) {
				smqClient.unsubscribe('TOPIC', `INTERNAL.REFRESH.${userId}`);
				smqClient.unsubscribe('TOPIC', `DIRECT.USER.${userId}`);
			}

			this.userId = undefined;
		}
	}

	async refreshInterestingValues(type: 'CONNECT' | 'DISCONNECT' | 'REFRESH') {
		const oldInterestingUserIds: string[] = this.interestingUserIds;
		const oldInterestingGroupIds: string[] = this.interestingGroupIds;
		this.interestingUserIds = [];
		this.interestingGroupIds = [];

		if (this.userId && type != 'DISCONNECT') {
			this.interestingUserIds.push(this.userId);
			const response = await axios.get(`http://${STACK_NAME}_chat-service:3000/groups`, {
				headers: {
					authorization: SERVICE_TOKEN,
					userId: this.userId,
				},
			});

			if (response != null && response.status == 200) {
				response.data.forEach((group: any) => {
					this.interestingGroupIds.push(group.id);
					if (group.type === 'DM') {
						this.interestingUserIds.push(group.partnerId);
					}
				});
			}
		}

		const removedInterestingGroupIds = oldInterestingGroupIds
			.filter((id) => !this.interestingGroupIds.includes(id))
			.filter((id) => !connections.find((c) => c.interestingGroupIds.includes(id)));

		const removedInterestingUserIds = oldInterestingUserIds
			.filter((id) => !this.interestingUserIds.includes(id))
			.filter((id) => !connections.find((c) => c.interestingUserIds.includes(id)));

		const addedInterestingGroupIds = this.interestingGroupIds.filter((id) => !oldInterestingGroupIds.includes(id));

		const addedInterestingUserIds = this.interestingUserIds.filter((id) => !oldInterestingUserIds.includes(id));

		const smqClient = service.getSMQClient();
		removedInterestingGroupIds.forEach((id) => smqClient.unsubscribe('TOPIC', `DIRECT.GROUP.${id}`));
		removedInterestingUserIds.forEach((id) => smqClient.unsubscribe('TOPIC', `PUBLIC.USER.${id}`));
		addedInterestingGroupIds.forEach((id) => smqClient.subscribe('TOPIC', `DIRECT.GROUP.${id}`));
		addedInterestingUserIds.forEach((id) => smqClient.subscribe('TOPIC', `PUBLIC.USER.${id}`));

		if (type === 'REFRESH') {
			await StatusHandler.onUserRefresh(this, removedInterestingUserIds, addedInterestingUserIds);
			await CallHandler.onUserRefresh(this, removedInterestingGroupIds, addedInterestingGroupIds);
		}
	}

	async onPublish(packet: { type: string; body?: any }) {
		if (this.userId && this.connectionId) {
			CallHandler.onEvent(this, packet);
		}
	}
}
