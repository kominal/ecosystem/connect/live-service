import { Packet } from '@kominal/service-util/model/packet';
import { info, error } from '@kominal/service-util/helper/log';
import service from '.';
import { connections, Connection } from './connection';
import { SocketConnectionDatabase } from '@kominal/connect-models/socketconnection/socketconnection.database';

export class SMQHandler {
	async onConnect(): Promise<void> {
		info('Connected to SwarmMQ cluster.');
		const smqClient = service.getSMQClient();
		if (smqClient) {
			smqClient.subscribe('QUEUE', 'INTERNAL.NOTIFICATION');
			connections
				.filter((c) => c.userId != undefined)
				.forEach((c) => {
					const { connectionId, userId } = c;
					smqClient.subscribe('TOPIC', `DIRECT.CONNECTION.${connectionId}`);
					smqClient.subscribe('TOPIC', `DIRECT.USER.${userId}`);
					smqClient.subscribe('TOPIC', `INTERNAL.REFRESH.${userId}`);
					c.interestingGroupIds.forEach((id) => smqClient.subscribe('TOPIC', `DIRECT.GROUP.${id}`));
					c.interestingUserIds.forEach((id) => smqClient.subscribe('TOPIC', `PUBLIC.USER.${id}`));
				});
		}
	}

	async onDisconnect(): Promise<void> {
		error('Lost connection to SwarmMQ cluster.');
	}

	async onMessage(packet: Packet): Promise<void> {
		const { destinationType, destinationName, event } = packet;

		if (destinationType === 'QUEUE' && destinationName === 'INTERNAL.NOTIFICATION') {
			if (event) {
				this.onNotification(event);
			}
		} else {
			let targetConnections: Connection[] = [];

			if (destinationType === 'TOPIC' && destinationName.startsWith('DIRECT.USER.')) {
				const userId = destinationName.split('.')[2];
				targetConnections = connections.filter((c) => c.userId === userId);
			} else if (destinationType === 'TOPIC' && destinationName.startsWith('DIRECT.CONNECTION.')) {
				const connectionId = destinationName.split('.')[2];
				targetConnections = connections.filter((c) => c.connectionId === connectionId);
			} else if (destinationType === 'TOPIC' && destinationName.startsWith('DIRECT.GROUP.')) {
				const groupId = destinationName.split('.')[2];
				targetConnections = connections.filter((c) => c.interestingGroupIds.includes(groupId));
			} else if (destinationType === 'TOPIC' && destinationName.startsWith('PUBLIC.USER.')) {
				const userId = destinationName.split('.')[2];
				targetConnections = connections.filter((c) => c.interestingUserIds.includes(userId));
			} else if (destinationType === 'TOPIC' && destinationName.startsWith('INTERNAL.REFRESH.')) {
				const userId = destinationName.split('.')[2];
				targetConnections = connections.filter((c) => c.userId === userId);
				targetConnections.forEach((c) => c.refreshInterestingValues('REFRESH'));
				return;
			}

			targetConnections.forEach((c) => c.socket.emit('EVENT', event));
		}
	}

	async onNotification(event: { type: string; body?: any }) {
		const { type, body } = event;
		if (!type && !body) {
			return;
		}
		if (type === 'MESSAGE.NEW') {
			const { userIds, groupId, messageId } = body;

			const pushUserIds: string[] = [];

			for (const userId of userIds) {
				if ((await SocketConnectionDatabase.countDocuments({ userId })) > 0) {
					service.getSMQClient()?.publish('TOPIC', `DIRECT.USER.${userId}`, 'MESSAGE.NEW', { groupId, messageId });
				} else {
					pushUserIds.push(userId);
				}
			}

			if (pushUserIds.length > 0) {
				service.getSMQClient()?.publish('QUEUE', 'INTERNAL.PUSH', 'MESSAGE.NEW', { userIds: pushUserIds, groupId, messageId });
			}
		}
	}
}
